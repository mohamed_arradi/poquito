//App ID for the skill
var APP_ID = "amzn1.ask.skill.86d072cf-26fe-4571-9df6-428c195086af";
/**
 * The AlexaSkill prototype and helper functions
 */
var AlexaSkill = require('./AlexaSkill');

var http = require('http');
var rp = require('request-promise');
var nodeGeocoder = require('node-geocoder');

var Poquito = function() {
    AlexaSkill.call(this, APP_ID);
};

// Internal Data

var listCategories = "[{\"id\":239902,\"parentCategoryId\":0,\"title\":\"Summer Clearance\",\"categoryId\":239902,\"sortIndex\":15,\"thumbnailUrl\":\"https:\/\/poqmbuat.blob.core.windows.net\/app145\/9593060-540.jpg?v=131382877124870000\",\"thumbnailWidth\":160,\"thumbnailHeight\":201,\"categoryType\":\"ListView\",\"hasSubCategory\":false,\"deeplinkUrl\":null,\"subCategories\":null},{\"id\":224649,\"parentCategoryId\":0,\"title\":\"New In\",\"categoryId\":224649,\"sortIndex\":14,\"thumbnailUrl\":\"https:\/\/poqmbuat.blob.core.windows.net\/app145\/9592880-540.jpg?v=131382877011330000\",\"thumbnailWidth\":160,\"thumbnailHeight\":201,\"categoryType\":\"ListView\",\"hasSubCategory\":true,\"deeplinkUrl\":null,\"subCategories\":null},{\"id\":223969,\"parentCategoryId\":0,\"title\":\"Accessories\",\"categoryId\":223969,\"sortIndex\":13,\"thumbnailUrl\":\"https:\/\/poqmbuat.blob.core.windows.net\/app145\/9593060-540.jpg?v=131382877124870000\",\"thumbnailWidth\":160,\"thumbnailHeight\":201,\"categoryType\":\"ListView\",\"hasSubCategory\":true,\"deeplinkUrl\":null,\"subCategories\":null},{\"id\":224488,\"parentCategoryId\":0,\"title\":\"Fashion\",\"categoryId\":224488,\"sortIndex\":12,\"thumbnailUrl\":\"https:\/\/poqmbuat.blob.core.windows.net\/app145\/9592754-540.jpg?v=131382876864070000\",\"thumbnailWidth\":160,\"thumbnailHeight\":201,\"categoryType\":\"ListView\",\"hasSubCategory\":true,\"deeplinkUrl\":null,\"subCategories\":null},{\"id\":224579,\"parentCategoryId\":0,\"title\":\"Lingerie\",\"categoryId\":224579,\"sortIndex\":11,\"thumbnailUrl\":\"https:\/\/poqmbuat.blob.core.windows.net\/app145\/9598229-540.jpg?v=131382862980530000\",\"thumbnailWidth\":160,\"thumbnailHeight\":201,\"categoryType\":\"ListView\",\"hasSubCategory\":true,\"deeplinkUrl\":null,\"subCategories\":null},{\"id\":224886,\"parentCategoryId\":0,\"title\":\"Shoes\",\"categoryId\":224886,\"sortIndex\":10,\"thumbnailUrl\":\"https:\/\/poqmbuat.blob.core.windows.net\/app145\/9601645-540.jpg?v=131382851397970000\",\"thumbnailWidth\":160,\"thumbnailHeight\":201,\"categoryType\":\"ListView\",\"hasSubCategory\":true,\"deeplinkUrl\":null,\"subCategories\":null},{\"id\":224884,\"parentCategoryId\":0,\"title\":\"Sale\",\"categoryId\":224884,\"sortIndex\":9,\"thumbnailUrl\":\"https:\/\/poqmbuat.blob.core.windows.net\/app145\/9595224-540.jpg?v=131382871675600000\",\"thumbnailWidth\":160,\"thumbnailHeight\":201,\"categoryType\":\"ListView\",\"hasSubCategory\":false,\"deeplinkUrl\":null,\"subCategories\":null},{\"id\":224099,\"parentCategoryId\":0,\"title\":\"Brand\",\"categoryId\":224099,\"sortIndex\":8,\"thumbnailUrl\":\"https:\/\/poqmbuat.blob.core.windows.net\/app145\/9592896-540.jpg?v=131382877032030000\",\"thumbnailWidth\":160,\"thumbnailHeight\":201,\"categoryType\":\"ListView\",\"hasSubCategory\":true,\"deeplinkUrl\":null,\"subCategories\":null},{\"id\":224656,\"parentCategoryId\":0,\"title\":\"Outlet\",\"categoryId\":224656,\"sortIndex\":7,\"thumbnailUrl\":\"https:\/\/poqmbuat.blob.core.windows.net\/app145\/9634142-540.jpg?v=131382626224530000\",\"thumbnailWidth\":160,\"thumbnailHeight\":201,\"categoryType\":\"ListView\",\"hasSubCategory\":true,\"deeplinkUrl\":null,\"subCategories\":null},{\"id\":224875,\"parentCategoryId\":0,\"title\":\"Promotion\",\"categoryId\":224875,\"sortIndex\":4,\"thumbnailUrl\":\"https:\/\/poqmbuat.blob.core.windows.net\/app145\/9601576-540.jpg?v=131382852271030000\",\"thumbnailWidth\":160,\"thumbnailHeight\":201,\"categoryType\":\"ListView\",\"hasSubCategory\":false,\"deeplinkUrl\":null,\"subCategories\":null},{\"id\":224876,\"parentCategoryId\":0,\"title\":\"Promotions\",\"categoryId\":224876,\"sortIndex\":3,\"thumbnailUrl\":\"https:\/\/poqmbuat.blob.core.windows.net\/app145\/9598167-540.jpg?v=131382862937970000\",\"thumbnailWidth\":160,\"thumbnailHeight\":201,\"categoryType\":\"ListView\",\"hasSubCategory\":true,\"deeplinkUrl\":null,\"subCategories\":null},{\"id\":224885,\"parentCategoryId\":0,\"title\":\"Seasonal Promotion\",\"categoryId\":224885,\"sortIndex\":2,\"thumbnailUrl\":\"https:\/\/poqmbuat.blob.core.windows.net\/app145\/9597080-540.jpg?v=131382867589070000\",\"thumbnailWidth\":160,\"thumbnailHeight\":201,\"categoryType\":\"ListView\",\"hasSubCategory\":false,\"deeplinkUrl\":null,\"subCategories\":null}]";

// Extend AlexaSkill
Poquito.prototype = Object.create(AlexaSkill.prototype);
Poquito.prototype.constructor = Poquito;

Poquito.prototype.eventHandlers.onSessionStarted = function(sessionStartedRequest, session) {
    console.log("Poquito onSessionStarted requestId: " + sessionStartedRequest.requestId +
        ", sessionId: " + session.sessionId);
};

Poquito.prototype.eventHandlers.onLaunch = function(launchRequest, session, response) {
    console.log("Poquito onLaunch requestId: " + launchRequest.requestId + ", sessionId: " + session.sessionId);
    var speechOutput = "Welcome to Poquito";
    var repromptText = "You can know the last trendy jeans !";
    response.ask(speechOutput, repromptText);
};

Poquito.prototype.eventHandlers.onSessionEnded = function(sessionEndedRequest, session) {
    console.log("Poquito onSessionEnded requestId: " + sessionEndedRequest.requestId +
        ", sessionId: " + session.sessionId);
};

Poquito.prototype.intentHandlers = {
    // register custom intent handlers
    "CategoryItent": function(intent, session, response) {

        getProductsFromCategories(response);
    },
    "AMAZON.StopIntent": function(intent, session, response) {
        response.ask("Ok see you soon", "");
    },
    "AMAZON.HelpIntent": function(intent, session, response) {
        response.ask("You can Ask : get me some blue shoes ", "");
    }
};

function getProductsFromCategories(response) {

    var json = JSON.parse(listCategories);

    var textToSpeech = "";

    for (var key in json) {

        var title = json[key]["title"];

        textToSpeech += title;
        textToSpeech += "<break time=\"0.3s\"";
    }

    response.tell("Available categories are " + textToSpeech);

    response.ask("Do you want to see a specific category ? ", "");
    response.listen()
}
// Create the handler that responds to the Alexa Request.
exports.handler = function(event, context) {

    var poquito = new Poquito();
    poquito.execute(event, context);
};